package com.example.demo.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
//@EnableJpaRepositories(basePackages = {"com.example.demo.repository"},
//		entityManagerFactoryRef = "oneEntityManagerFactory",
//		  transactionManagerRef = "oneTransactionManager"
//)
public class DBConfigTenantOne {
	

	
	@Primary
	@Bean
	@ConfigurationProperties("spring.datasource.one")
	public DataSourceProperties oneDataSourceProperties() {
		return new DataSourceProperties();
	}
	
	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource.one")
	public DataSource oneDataSource() {
		return oneDataSourceProperties().initializeDataSourceBuilder().build();
	}
	
	
	@Primary
	@Bean(name = "oneEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean oneEntityManagerFactory(EntityManagerFactoryBuilder builder
			, @Qualifier("oneDataSource") DataSource dataSource) {
		return builder.dataSource(dataSource)
				.packages("com.example.demo.model")
				.persistenceUnit("TenantOne")
				.build();
	}

	@Primary
	@Bean(name = "oneTransactionManager")
	public PlatformTransactionManager oneTransactionManager(
			@Qualifier("oneEntityManagerFactory") EntityManagerFactory oneEntityManagerFactory) {
		return new JpaTransactionManager(oneEntityManagerFactory);
	}
	
}
