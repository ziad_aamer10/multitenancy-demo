package com.example.demo.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
//@EnableJpaRepositories(basePackages = {"com.example.demo.repository"},
//		entityManagerFactoryRef = "twoEntityManagerFactory",
//		  transactionManagerRef = "twoTransactionManager"
//)
public class DBConfigTenantTwo {
	
	
	
	@Bean
	@ConfigurationProperties("spring.datasource.two")
	public DataSourceProperties twoDataSourceProperties() {
		return new DataSourceProperties();
	}
	@Bean
	@ConfigurationProperties("spring.datasource.two")
	public DataSource twoDataSource() {
		return twoDataSourceProperties().initializeDataSourceBuilder().build();
	}
	
	@Bean(name = "twoEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean twoEntityManagerFactory(EntityManagerFactoryBuilder builder
			, @Qualifier("twoDataSource") DataSource dataSource) {
		return builder.dataSource(dataSource)
				.packages("com.example.demo.model")
				.persistenceUnit("TenantTwo")
				.build();
	}
	@Bean(name = "twoTransactionManager")
	public PlatformTransactionManager twoTransactionManager(
			@Qualifier("twoEntityManagerFactory") EntityManagerFactory twoEntityManagerFactory) {
		return new JpaTransactionManager(twoEntityManagerFactory);
	}

}
