package com.example.demo.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.example.demo.TenantAwareRoutingSource;

//EnableJpaRepositories for multi tenant
@Configuration
public class DBConfig {
	
	@Autowired
	private DBConfigTenantOne one;
	@Autowired
	private DBConfigTenantTwo two;
	
	@Bean
	public DataSource dataSource() {

		AbstractRoutingDataSource dataSource = new TenantAwareRoutingSource();

		Map<Object,Object> targetDataSources = new HashMap<>();

		targetDataSources.put("one", one.oneDataSource());
		targetDataSources.put("two", two.twoDataSource());

		dataSource.setTargetDataSources(targetDataSources);
		
		dataSource.afterPropertiesSet();

		return dataSource;
	}


}
