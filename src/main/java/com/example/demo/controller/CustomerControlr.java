package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.ThreadLocalStorage;
import com.example.demo.model.Customer;
import com.example.demo.repository.CustomerRepository;

@RestController
@RequestMapping("/customers")
public class CustomerControlr {

	@Autowired
	private CustomerRepository customerRepo;
	
	
	@GetMapping("/one")
	public List<Customer> getAllCustomers1(){
		ThreadLocalStorage.setTenantName("one");
		return customerRepo.findAll();
	}
	
	@PostMapping("/one")
	public Customer saveCustomer1(@RequestBody Customer customer){
		ThreadLocalStorage.setTenantName("one");
		return customerRepo.save(customer);
	}
	
	@GetMapping("/two")
	public List<Customer> getAllCustomers2(){
		ThreadLocalStorage.setTenantName("two");
		return customerRepo.findAll();
	}
	
	@PostMapping("/two")
	public Customer saveCustomer2(@RequestBody Customer customer){
		ThreadLocalStorage.setTenantName("two");
		return customerRepo.save(customer);
	}
	
	
	
}
