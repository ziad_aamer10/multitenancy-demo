package com.example.demo;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.example.demo.config.ThreadLocalStorage;

public class TenantAwareRoutingSource extends AbstractRoutingDataSource{

    @Override
    protected Object determineCurrentLookupKey() {
        return ThreadLocalStorage.getTenantName();
    }

}
