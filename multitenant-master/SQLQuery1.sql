CREATE TABLE DATASOURCECONFIG (
	id bigint PRIMARY KEY,
	driverclassname VARCHAR(255),
	url VARCHAR(255),
	name VARCHAR(255),
	username VARCHAR(255),
	password VARCHAR(255),
	initialize bit
);

##### Schema Creation ############
create schema test1;
create schema test2;
create table test1.city(id bigint, name varchar(200));
create table test2.city(id bigint, name varchar(200));

CREATE SEQUENCE "test1".hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE "test2".hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
INSERT INTO DATASOURCECONFIG VALUES (1, 'com.microsoft.sqlserver.jdbc.SQLServerDriver', 'jdbc:sqlserver://196.221.145.198:1533;databaseName=DG_TENANT_ONE?currentSchema=test1&ApplicationName=MultiTenant', 'test1', 'aml', 'amlaml', 1);
INSERT INTO DATASOURCECONFIG VALUES (2, 'com.microsoft.sqlserver.jdbc.SQLServerDriver', 'jdbc:sqlserver://196.221.145.198:1533;databaseName=DG_TENANT_ONE?currentSchema=test2&ApplicationName=MultiTenant', 'test2', 'aml', 'amlaml', 1);


select * from city 