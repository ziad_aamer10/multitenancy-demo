package com.example.entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "REPORT")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int rentityId;

    private String rentityBranch;

    @Column(updatable=false)
    private String submissionCode;

    @Column(updatable=false)
    private String reportCode;

    private String entityReference;

    private String fiuRefNumber;

    private String submissionDate;

    @Column(updatable=false)
    private String currencyCodeLocal;

    private String reportingPersonType;

    private String location;


    @Column(length = 4000)
    private String reason;

    private String action;



    private String reportStatusCode ;

    @Column(updatable=false)
    private String reportCreatedDate ;

    @Column(updatable=false)
    private String reportCreatedBy;

    private String reportClosedDate ;
    //owner
    private String reportUserLockId ;


    private String priority;

    private String reportRiskSignificance ;
    private String version ;
    private String reportXml ;
    private String isValid ;



    public Report(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRentityId() {
        return rentityId;
    }

    public void setRentityId(int rentityId) {
        this.rentityId = rentityId;
    }

    public String getRentityBranch() {
        return rentityBranch;
    }

    public void setRentityBranch(String rentityBranch) {
        this.rentityBranch = rentityBranch;
    }

    public String getSubmissionCode() {
        return submissionCode;
    }

    public void setSubmissionCode(String submissionCode) {
        this.submissionCode = submissionCode;
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getEntityReference() {
        return entityReference;
    }

    public void setEntityReference(String entityReference) {
        this.entityReference = entityReference;
    }

    public String getFiuRefNumber() {
        return fiuRefNumber;
    }

    public void setFiuRefNumber(String fiuRefNumber) {
        this.fiuRefNumber = fiuRefNumber;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(String submissionDate) {
        this.submissionDate = submissionDate;
    }

    public String getCurrencyCodeLocal() {
        return currencyCodeLocal;
    }

    public void setCurrencyCodeLocal(String currencyCodeLocal) {
        this.currencyCodeLocal = currencyCodeLocal;
    }

    public String getReportingPersonType() {
        return reportingPersonType;
    }

    public void setReportingPersonType(String reportingPersonType) {
        this.reportingPersonType = reportingPersonType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


    public String getReportStatusCode() {
        return reportStatusCode;
    }

    public void setReportStatusCode(String reportStatusCode) {
        this.reportStatusCode = reportStatusCode;
    }

    public String getReportCreatedDate() {
        return reportCreatedDate;
    }

    public void setReportCreatedDate(String reportCreatedDate) {
        this.reportCreatedDate = reportCreatedDate;
    }

    public String getReportClosedDate() {
        return reportClosedDate;
    }

    public void setReportClosedDate(String reportClosedDate) {
        this.reportClosedDate = reportClosedDate;
    }

    public String getReportUserLockId() {
        return reportUserLockId;
    }

    public void setReportUserLockId(String reportUserLockId) {
        this.reportUserLockId = reportUserLockId;
    }

    public String getReportRiskSignificance() {
        return reportRiskSignificance;
    }

    public void setReportRiskSignificance(String reportRiskSignificance) {
        this.reportRiskSignificance = reportRiskSignificance;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getReportXml() {
        return reportXml;
    }

    public void setReportXml(String reportXml) {
        this.reportXml = reportXml;
    }

    public String getIsValid() {
        return isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    public String getReportCreatedBy() {
        return reportCreatedBy;
    }

    public void setReportCreatedBy(String reportCreatedBy) {
        this.reportCreatedBy = reportCreatedBy;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }



    @Override
    public String toString() {
        return "Report [id=" + id + ", rentityId=" + rentityId + ", rentityBranch=" + rentityBranch
                + ", submissionCode=" + submissionCode + ", reportCode=" + reportCode + ", entityReference="
                + entityReference + ", fiuRefNumber=" + fiuRefNumber + ", submissionDate=" + submissionDate
                + ", currencyCodeLocal=" + currencyCodeLocal + ", reportingPersonType=" + reportingPersonType
                + ", location=" + location + ", reason=" + reason + ", action=" + action + ", transaction="
                + ", reportStatusCode=" + reportStatusCode + ", reportCreatedDate=" + reportCreatedDate
                + ", reportClosedDate=" + reportClosedDate + ", reportUserLockId=" + reportUserLockId + ", priority="
                + priority + ", reportRiskSignificance=" + reportRiskSignificance + ", version=" + version
                + ", reportXml=" + reportXml + ", isValid=" + isValid + ", reportCreatedBy=" + reportCreatedBy + "]";

    }





}
